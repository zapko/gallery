# Gallery

Simple gallery app


## Qualities
- Expandable
- Modular
- Backend-interchangable
- Localizable
- Unit-testable
- Only 1 dependency (PromiseKit)