//
//  StorefrontCell.swift
//  Gallery
//
//  Created by Konstantin Zabelin on 27.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import UIKit

class StorefrontCell: UICollectionViewCell {

	@IBOutlet var title: 	UILabel!
	@IBOutlet var subtitle: UILabel!

	@IBOutlet var imageView: UIImageView!
}
