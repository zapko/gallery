//
// Created by Konstantin Zabelin on 26.05.2018.
// Copyright (c) 2018 Konstantin Zabelin. All rights reserved.
//

import Foundation
import UIKit


class StorefrontRouterImplementation: StorefrontRouter {


	// MARK: - Private properties

	private weak var viewController: UIViewController?

	private var actionPicker: UIViewController?


	// MARK: - Initialization / Deinitialization

	init(viewController: UIViewController) {
		self.viewController = viewController
	}


	// MARK: - StorefrontRouter

	func showShareScreen(imageData: Data, url: URL) {

		guard let image = UIImage(data: imageData) else { return }

		let activityViewControllerToPresent = UIActivityViewController(
			activityItems: [image, url],
			applicationActivities: nil
		)

		guard let view = viewController else {
			assertionFailure("View controller is missing")
			return
		}

		view.present(activityViewControllerToPresent, animated: true)
		actionPicker = activityViewControllerToPresent
	}

	func dismissShareScreen() {

		guard let presentedActivityViewController = actionPicker else {
			assertionFailure("Being asked te dismiss share view controller, although it's not registered in the router")
			return
		}

		presentedActivityViewController.dismiss(animated: true)
		actionPicker = nil
	}

	func showSortPicker(onPick: @escaping (Storefront.Sort) -> Void) {

		guard let view = viewController else {
			assertionFailure("View controller is missing")
			return
		}

		let picker = UIAlertController(
			title: 			nil,
			message: 		nil,
			preferredStyle: .actionSheet
		)

		let dateTaken = UIAlertAction(
			title: Storefront.localize("sort-by.date-taken"),
			style: .default
		) {
			_ in onPick(.byDateTaken)
		}

		let datePublished = UIAlertAction(
			title: Storefront.localize("sort-by.date-published"),
			style: .default
		) {
			_ in onPick(.byDatePublished)
		}

		picker.addAction(dateTaken)
		picker.addAction(datePublished)

		view.present(picker, animated: true)
	}

	func showSearchStringPicker(onPick: @escaping (String) -> Void) {

		guard let view = viewController else {
			assertionFailure("View controller is missing")
			return
		}

		let picker = UIAlertController(
			title: 			Storefront.localize("search.title"),
			message: 		nil,
			preferredStyle: .alert
		)

		picker.addTextField {
			textField in
			textField.placeholder = Storefront.localize("search.placeholder")
		}

		let search = UIAlertAction(
			title: Storefront.localize("search.action"),
			style: .default
		) {
			[weak picker] _ in
			onPick(picker!.textFields!.first!.text!)
		}

		picker.addAction(search)

		view.present(picker, animated: true)
	}
}
