//
//  StorefrontLogic.swift
//  Gallery
//
//  Created by Konstantin Zabelin on 26.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import Foundation
import PromiseKit


class StorefrontLogic: StorefrontViewOutput {


	// MARK: - Private properties

	private weak var view: StorefrontView?
	private let router: StorefrontRouter

	private let imageService: ImageService

	private var sortMethod: Storefront.Sort = .byDatePublished {
		didSet { updateData() }
	}

	private var searchString: String = "" {
		didSet { updateData() }
	}


	// MARK: - Initialization / Deinitialization

	init(
		view: 		  StorefrontView,
		router: 	  StorefrontRouter,
		imageService: ImageService
	) {
		self.view 		  = view
		self.router		  = router
		self.imageService = imageService
	}


	// MARK: - StorefrontViewOutput

	func viewDidPrepare() {
		updateData()
	}

	func didPressShare(on image: Storefront.Image) {

		guard let existingView = view else { return	}

		let preShareViewState = existingView.state

		existingView.state = .loading
		image
			.data
			.done {
				[router] in
				existingView.state = preShareViewState
				router.showShareScreen(imageData: $0, url: URL(string: image.id)!)
			}
			.catch {
				existingView.state = .error($0)
			}
	}

	func didPressSortBy() {

		router.showSortPicker {
			[weak self] newSortMethod in
			self?.sortMethod = newSortMethod
		}
	}

	func didPressSearch() {

		router.showSearchStringPicker {
			[weak self] newSearchString in
			self?.searchString = newSearchString
		}
	}

	func didPressRetry() {
		updateData()
	}


	// MARK: - Private Methods

	private func updateData() {

		guard let liveView = view else { return }

		liveView.state = .loading
		imageService
			.fetchImageData(searchString: searchString)
			.map(on: .global()) {
				[sortMethod]
				imageData -> [ImageData] in

				let comparator: (ImageData, ImageData) -> Bool
				switch sortMethod {
				case .byDatePublished: comparator = { lhs, rhs in lhs.published > rhs.published	}
				case .byDateTaken: 	   comparator = { lhs, rhs in lhs.taken 	> rhs.taken		}
				}

				return imageData.sorted(by: comparator)
			}
			.map(on: .global()) {
				[imageService] in
				$0.map {
					Storefront.Image.init(
						imageData: $0,
						dataPromise: imageService.downloadFrom(url: $0.url)
					)
				}
			}
			.done  {
				[weak view] in
				view?.state = .displaying($0)
			}
			.catch {
				[weak view] in
				view?.state = .error($0)
				print("Error: \($0)")
			}
	}

}

private extension Storefront.Image {

	init(imageData: ImageData, dataPromise: Promise<Data>) {
		self.id  	  = imageData.id
		self.url 	  = imageData.url
		self.data	  = dataPromise
		self.title 	  = imageData.title
		self.subtitle = imageData.author
	}
}
