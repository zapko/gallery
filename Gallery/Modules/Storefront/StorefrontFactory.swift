//
//  StorefrontFactory.swift
//  Gallery
//
//  Created by Konstantin Zabelin on 26.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import UIKit


class StorefrontFactoryImplementation: StorefrontFactory {


	// MARK: - StorefrontFactory

	func storefront() -> UIViewController {

		let view = StorefrontViewController(
			nibName: "StorefrontViewController",
			bundle:  nil
		)

		let router = StorefrontRouterImplementation(viewController: view)

		let flickerAPI   = FlickerAPI()
		let imageService = FlickerService(api: flickerAPI)

		let coordinator = StorefrontLogic(
			view: 		  view,
			router: 	  router,
			imageService: imageService
		)

		view.output = coordinator

		return view
	}
}
