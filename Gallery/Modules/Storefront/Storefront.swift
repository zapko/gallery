//
//  Storefront.swift
//  Gallery
//
//  Created by Konstantin Zabelin on 26.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit


struct Storefront: Localizer {

	struct Image: Equatable {

		typealias ID = ImageData.ID

		let id: 	  ID
		let url: 	  URL
		let data:	  Promise<Data>
		let title: 	  String
		let subtitle: String
	}

	enum Sort {
		case byDateTaken
		case byDatePublished
	}

	enum ViewState {
		case loading
		case displaying([Image])
		case error(Error)
	}
}


// MARK: - External Interfaces

protocol StorefrontFactory: class {
	func storefront() -> UIViewController
}


// MARK: - Internal Interfaces

protocol StorefrontView: class {
	var state: Storefront.ViewState { get set }
}

protocol StorefrontViewOutput: class {

	func viewDidPrepare()

	func didPressShare(on: Storefront.Image)

	func didPressSortBy()
	func didPressSearch()

	func didPressRetry()
}

protocol StorefrontRouter: class {

	func showShareScreen(imageData: Data, url: URL)
	func dismissShareScreen()

	func showSortPicker(onPick: @escaping (Storefront.Sort) -> Void)
	func showSearchStringPicker(onPick: @escaping (String) -> Void)
}


// MARK: - Model Extensions

func ==(lhs: Storefront.Image, rhs: Storefront.Image) -> Bool {
	return lhs.id == rhs.id
}
