//
//  StorefrontViewController.swift
//  Gallery
//
//  Created by Konstantin Zabelin on 26.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import UIKit
import PromiseKit


class StorefrontViewController: UIViewController,
								UICollectionViewDelegateFlowLayout,
								UICollectionViewDelegate,
								UICollectionViewDataSource,
								StorefrontView {


	// MARK: - Constants

	struct Constants {
		static let cellName = "\(StorefrontCell.self)"
		static let verticalSpacing: CGFloat = 2
		static let maxItemSide: CGFloat = 210
	}


	// MARK: - View Outlets

	@IBOutlet var collectionView: UICollectionView!

	@IBOutlet var errorStub: UIView!
	@IBOutlet var errorDescription: UILabel!

	@IBOutlet var loaderStub: UIView!


	// MARK: - Private properties

	private var items: [Storefront.Image] = [] {
		didSet {

			if items == oldValue { return }

			collectionView.reloadData()
		}
	}

	private var cellSide: CGFloat = Constants.maxItemSide


	// MARK: - StorefrontViewController

	var output: StorefrontViewOutput!


	// MARK: - UIViewController

	override func viewDidLoad() {
		title = Storefront.localize("title")

		collectionView.register(
			UINib(
				nibName: Constants.cellName,
				bundle: nil
			),
			forCellWithReuseIdentifier: Constants.cellName
		)

		navigationItem.leftBarButtonItem = UIBarButtonItem(
			title: Storefront.localize("nav-actions.search"),
			style: .plain,
			target: self,
			action: #selector(searchButtonPressed)
		)

		navigationItem.rightBarButtonItem = UIBarButtonItem(
			title: Storefront.localize("nav-actions.sort"),
			style: .plain,
			target: self,
			action: #selector(sortButtonPressed)
		)

		output.viewDidPrepare()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		cellSide = calculateCellSide(for: view.bounds.width)
	}

	override func viewWillTransition(
		to size: CGSize,
		with coordinator: UIViewControllerTransitionCoordinator
	) {
		super.viewWillTransition(to: size, with: coordinator)

		cellSide = calculateCellSide(for: size.width)

		(collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.invalidateLayout()
	}


	// MARK: - CollectionViewDelegateFlowLayout

	func collectionView(
		_ collectionView: UICollectionView,
		layout collectionViewLayout: UICollectionViewLayout,
		sizeForItemAt indexPath: IndexPath
	) -> CGSize {

		return CGSize(width: cellSide, height: cellSide)
	}


	// MARK: - CollectionViewDelegate

	func collectionView(
		_ collectionView: UICollectionView,
		didSelectItemAt indexPath: IndexPath
	) {

		guard case .displaying(let images) = state else {
			assertionFailure("Trying to display cell in not displaying state")
			return
		}

		let image = images[indexPath.row]

		output.didPressShare(on: image)
	}


	// MARK: - CollectionViewDataSource

	func collectionView(
		_ collectionView: UICollectionView,
		numberOfItemsInSection section: Int
	) -> Int {
		return items.count
	}

	func collectionView(_ collectionView: UICollectionView,
		cellForItemAt indexPath: IndexPath
	) -> UICollectionViewCell {

		let storefrontCell: StorefrontCell
		if let cell = collectionView.dequeueReusableCell(
			withReuseIdentifier: Constants.cellName,
			for: indexPath
		) as? StorefrontCell {
			storefrontCell = cell
		} else {
			storefrontCell = StorefrontCell()
		}

		guard case .displaying(let images) = state else {
			assertionFailure("Trying to display cell in not displaying state")
			return storefrontCell
		}

		let image = images[indexPath.row]

		storefrontCell.title.text 	   = image.title
		storefrontCell.subtitle.text   = image.subtitle
		storefrontCell.imageView.image = nil
		image
			.data
			.map(on: .global(), UIImage.init(data:))
			.done {	storefrontCell.imageView.image = $0	}
			.catch {
				print("Error while setting image at \(indexPath): \($0)")
			}

		return storefrontCell
	}


	// MARK: - StorefrontView

	var state: Storefront.ViewState = .displaying([]) {
		didSet {

			let errorStubAlpha:  CGFloat
			let loaderStubAlpha: CGFloat

			switch state {
			case .loading:
				errorStubAlpha  = 0
				loaderStubAlpha = 1

			case .error(let error):
				errorStubAlpha  = 1
				loaderStubAlpha = 0
				errorDescription.text = error.localizedDescription

			case .displaying(let images):
				errorStubAlpha  = 0
				loaderStubAlpha = 0
				items = images
			}

			UIView.animate(
				withDuration: 0.3,
				delay: 		  0,
				options: 	  .beginFromCurrentState,
				animations:   {
					[errorStub, loaderStub] in
					errorStub?.alpha  = errorStubAlpha
					loaderStub?.alpha = loaderStubAlpha
				},
				completion: nil
			)
		}
	}


	// MARK: - Actions

	@IBAction func retryButtonPressed() {
		output.didPressRetry()
	}

	@objc private func sortButtonPressed() {
		output.didPressSortBy()
	}

	@objc private func searchButtonPressed() {
		output.didPressSearch()
	}
}

private func calculateCellSide(for width: CGFloat) -> CGFloat {

	var side = width
	var i = 0
	while side > StorefrontViewController.Constants.maxItemSide {
		i += 1
		side = floor((width - CGFloat(i - 1) * StorefrontViewController.Constants.verticalSpacing) / CGFloat(i))
	}
	return side
}
