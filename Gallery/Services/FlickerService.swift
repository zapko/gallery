//
// Created by Konstantin Zabelin on 28.05.2018.
// Copyright (c) 2018 Konstantin Zabelin. All rights reserved.
//

import Foundation
import PromiseKit


class FlickerService: ImageService {


	// MARK: - Private properties

	private let api: API


	// MARK: - Initialization / Deinitialization

	init(api: API) {
		self.api = api
	}


	// MARK: - ImageService

	func fetchImageData(searchString: String) -> Promise<[ImageData]> {

		return firstly {
			api.sendRequest(to: PublicEndpoint.feed(search: searchString)) as Promise<Feed>
		}
		.map(on: .global()) {
			try $0.items.map(ImageData.init(flickerPhoto:))
		}
	}

	func downloadFrom(url: URL) -> Promise<Data> {

		return Promise {
			resolver in

			let dataTask = URLSession.shared.dataTask(with: url) {
				resolver.resolve($0, $2)
			}

			dataTask.resume()
		}
	}
}

private struct Photo: Decodable {

	let link:  String
	let media: Media

	let title:  String
	let author: String

	let dateTaken: Date
	let published: Date

	private enum CodingKeys: String, CodingKey {
		case link, title, author, dateTaken = "date_taken", published, media
	}
}

private struct Media: Decodable {
	let m: String
}

private struct Feed: Decodable {

	let title: String
	let items: [Photo]
}

private extension ImageData {

	init(flickerPhoto photo: Photo) throws {

		guard let photoURL = URL(string: photo.media.m) else {
			throw ImageServiceError.invalidURL(photo.media.m)
		}

		self.id    	   = photo.link
		self.url   	   = photoURL
		self.title 	   = photo.title
		self.author    = photo.author
		self.taken 	   = photo.dateTaken
		self.published = photo.published
	}
}
