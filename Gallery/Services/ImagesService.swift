//
//  ImagesService.swift
//  Gallery
//
//  Created by Konstantin Zabelin on 26.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import Foundation
import PromiseKit


struct ImageData {

	typealias ID = String

	let id:  ID
	let url: URL

	let title:  String
	let author: String

	let taken: 	   Date
	let published: Date
}

enum ImageServiceError: Error {
	case invalidURL(String)
}


protocol ImageService {
	func fetchImageData(searchString: String) -> Promise<[ImageData]>
	func downloadFrom(url: URL) -> Promise<Data> // TODO: extract into different service
}


