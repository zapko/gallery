//
// Created by Konstantin Zabelin on 26.05.2018.
// Copyright (c) 2018 Konstantin Zabelin. All rights reserved.
//

import Foundation
import PromiseKit


protocol Endpoint {
	var url: URL { get }
}

enum APIError: Error {
	case responseContainsNoData
	case dataTransformationFailed(String)
}

protocol API {
	func sendRequest<T:Decodable>(to endpoint: Endpoint) -> Promise<T>
}

