//
//  FlickerAPI.swift
//  Gallery
//
//  Created by Konstantin Zabelin on 26.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import Foundation
import PromiseKit


enum PublicEndpoint: Endpoint {

	case feed(search: String)


	// MARK: - Endpoint

	var url: URL { // TODO: compose out of URLComponents
		switch self {
		case .feed(let searchString):

			let base = "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1"

			if !searchString.isEmpty,
				let screenedSearchString = searchString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
				return URL(string: base + "&tags=\(screenedSearchString)")!
			} else {
				return URL(string: base)!
			}
		}
	}
}


class FlickerAPI: API {


	// MARK: - Private properties

	private let decoder: JSONDecoder = {

		let decoder = JSONDecoder()
		decoder.dateDecodingStrategy = .iso8601

		return decoder
	}()


	// MARK: - API

	func sendRequest<T:Decodable>(to endpoint: Endpoint) -> Promise<T> {

		return Promise {
			resolver in

			let task = URLSession.shared.dataTask(with: endpoint.url) {
				[resolver]
				possibleData, possibleResponse, possibleError in

				resolver.resolve(possibleData, possibleError)
			}

			task.resume()

			}
			.map(on: .global()) {
				[decoder] data -> T in
				try decoder.decode(T.self, from: data)
			}
	}
}
