//
// Created by Konstantin Zabelin on 29.05.2018.
// Copyright (c) 2018 Konstantin Zabelin. All rights reserved.
//

import Foundation

protocol Localizer {
	static func localize(_ key: String) -> String
	static func localizationTable() -> String
}

extension Localizer {

	static func localizationTable() -> String {
		return "\(self)"
	}

	static func localize(_ key: String) -> String {
		return NSLocalizedString(
			key,
			tableName: localizationTable(),
			comment: key
		)
	}
}