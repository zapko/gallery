//
//  Result.swift
//  Gallery
//
//  Created by Konstantin Zabelin on 26.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import Foundation

enum Result<T> {
	case success(T)
	case failure(Error)
}
