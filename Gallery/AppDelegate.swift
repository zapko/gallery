//
//  AppDelegate.swift
//  Gallery
//
//  Created by Konstantin Zabelin on 26.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(
		_ application: UIApplication,
		didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
	) -> Bool {

		let storefrontFactory = StorefrontFactoryImplementation()

		let storefront = storefrontFactory.storefront()

		let navigationController = UINavigationController(rootViewController: storefront)

		let newWindow = UIWindow()
		newWindow.rootViewController = navigationController
		newWindow.makeKeyAndVisible()
		self.window = newWindow

		return true
	}
}

