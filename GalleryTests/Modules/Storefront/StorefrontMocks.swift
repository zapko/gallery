//
//  StorefrontMocks.swift
//  GalleryTests
//
//  Created by Konstantin Zabelin on 29.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import XCTest
@testable import Gallery


class StorefrontViewMock: StorefrontView {

	var t_stateSet: [Storefront.ViewState] = []
	var t_stateSetHandler: (([Storefront.ViewState]) -> Void)?

	var state: Storefront.ViewState {
		didSet {
			t_stateSet.append(state)
			t_stateSetHandler?(t_stateSet)
		}
	}

	init(state: Storefront.ViewState) {
		self.state = state
	}
}

class StorefrontRouterMock: StorefrontRouter {

	func showShareScreen(imageData: Data, url: URL) {

	}

	func dismissShareScreen() {

	}

	func showSortPicker(onPick: @escaping (Storefront.Sort) -> Void) {

	}

	func showSearchStringPicker(onPick: @escaping (String) -> Void) {

	}
}
