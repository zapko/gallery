//
//  StorefrontLogicTests.swift
//  GalleryTests
//
//  Created by Konstantin Zabelin on 29.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import XCTest
import PromiseKit
@testable import Gallery

class StorefrontLogicTests: XCTestCase {

	var logic: StorefrontLogic!

	var viewMock: 		  StorefrontViewMock!
	var routerMock: 	  StorefrontRouterMock!
	var imageServiceMock: ImageServiceMock!

	override func setUp() {
		super.setUp()

		viewMock   		 = StorefrontViewMock(state: .displaying([]))
		routerMock 		 = StorefrontRouterMock()
		imageServiceMock = ImageServiceMock()

		logic = StorefrontLogic(
			view: 		  viewMock,
			router: 	  routerMock,
			imageService: imageServiceMock
		)
	}

	func test_When_view_is_prepared_data_loading_starts() {

		imageServiceMock.t_fetchImageDataResult = .value([])

		logic.viewDidPrepare()

		XCTAssertGreaterThan(imageServiceMock.t_fetchImageDataCalledCounter, 0)
	}

	func test_When_data_loading_starts_view_is_switched_into_loading_state() {

		imageServiceMock.t_fetchImageDataResult = .value([])

		logic.viewDidPrepare()

		guard case .some(Storefront.ViewState.loading) = viewMock.t_stateSet.first else {
			XCTFail("View state was set to _loading_ at first")
			return
		}
	}

	func test_When_data_loading_finishes_successfully_view_is_switched_into_displaying_state() {

		let loadingFinishes = expectation(description: "Loading will finish eventually")

		viewMock.t_stateSetHandler = {
			states in

			guard case Storefront.ViewState.loading = states[0] else {
				XCTFail("View state is set to _loading_ at first")
				return
			}

			if states.count == 1 { return }

			guard case Storefront.ViewState.displaying = states[1] else {
				XCTFail("View state is set to _displaying_ after successful loading")
				return
			}

			loadingFinishes.fulfill()
		}

		imageServiceMock.t_fetchImageDataResult = .value([])


		logic.viewDidPrepare()
		
		wait(for: [loadingFinishes], timeout: 3)
	}

	func test_When_data_loading_finishes_with_error_view_is_switched_into_error_state() {

		let loadingFinishes = expectation(description: "Loading will finish eventually")

		viewMock.t_stateSetHandler = {
			states in

			guard case Storefront.ViewState.loading = states[0] else {
				XCTFail("View state is set to _loading_ at first")
				return
			}

			if states.count == 1 { return }

			guard case Storefront.ViewState.error(let error) = states[1] else {
				XCTFail("View state is set to _error_ after loading failure")
				return
			}

			guard let testError = error as? TestError, case TestError.failureResult = testError else {
				XCTFail("View is displaying the same error, that it receives from ImageService")
				return
			}

			loadingFinishes.fulfill()
		}

		imageServiceMock.t_fetchImageDataResult = Promise(error: TestError.failureResult)


		logic.viewDidPrepare()

		wait(for: [loadingFinishes], timeout: 3)
	}
}
