//
//  ImageServiceMock.swift
//  GalleryTests
//
//  Created by Konstantin Zabelin on 29.05.2018.
//  Copyright © 2018 Konstantin Zabelin. All rights reserved.
//

import XCTest
import PromiseKit
@testable import Gallery

enum TestError: Error {
	case notSetProperly
	case failureResult
}

class ImageServiceMock: ImageService {

	var t_fetchImageDataResult: Promise<[ImageData]>?
	var t_fetchImageDataCalledCounter: Int = 0

	func fetchImageData(searchString: String) -> Promise<[ImageData]> {

		t_fetchImageDataCalledCounter += 1

		guard let result = t_fetchImageDataResult else {
			XCTFail("Test is not set up properly")
			return Promise(error: TestError.notSetProperly)
		}

		return result
	}

	var t_downloadFromResult: Promise<Data>?

	func downloadFrom(url: URL) -> Promise<Data> {

		guard let result = t_downloadFromResult else {
			XCTFail("Test is not set up properly")
			return Promise(error: TestError.notSetProperly)
		}

		return result
	}
}
